# R script to analyze data extracted from tweets containing a specific word

# libraries
library(plyr)
library(ggplot2)
library(gridExtra)
# load the data
myData <- read.csv("/home/norm/Softwares/git/python-twitter/Twitter-ASV2015/results.csv",
                   header=TRUE,
                   as.is=TRUE,
                   nrows=3000)

# stats on data
nb_tweets <- (nrow(myData))
list_users <- unique(myData$Username)
nb_unique_users <- length(list_users)

freq <- count(myData,'Username')
freq_sorted <- arrange(freq, -freq)


# plot top 20
x20 <- freq_sorted[1:20,1]
y20 <- freq_sorted[1:20,2]
df <- data.frame(User = x20, Count = y20)

#g1 <- ggplot(df, aes(x = User, y = Count)) + geom_bar(stat = "identity")
graph <- ggplot(df, aes(x = reorder(User, -Count), y = Count)) + ylim(0,510)
gtype <- geom_bar(stat = "identity", fill="#4F2683")
glabels <- labs(title="Top 20 #ASV2105 tweeters", x="User", y="Number of tweet")
gtext <- geom_text(aes(label = y20), vjust=-1, position = position_dodge(0.9), size = 3, colour="#4F2683")
gtheme <- theme(axis.text.x=element_text(angle=60, hjust=1, size=12),
                axis.title.x=element_blank(),
                panel.grid.major.x=element_blank(),
                axis.ticks.x=element_blank(),
                axis.ticks.y=element_blank(),
                panel.grid.major.y=element_line(colour="#4F2683", linetype="dotted", size=0.2),
                plot.title=element_text(vjust=1, size=20),
                axis.title.y=element_text(size=16),
                axis.text.y=element_text(hjust=1.25, size=12)
)
gannotate1 <- annotate("text", x=1, y=500, label=y20[1], size=4, colour="#4F2683")
gannotate2 <- annotate("text", x=20, y=50, label=y20[20], size=4, colour="#4F2683")

g_top20 <- graph + gtype + glabels + gtheme + gannotate1 + gannotate2
grid.arrange(arrangeGrob(g_top20))

